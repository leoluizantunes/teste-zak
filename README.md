# Teste-Zak

O projeto em questão foi realizaado utilizando Cypress e Cucumber, os reports estão sendo criados através do Mochawesome.
Em caso de dúvidas, mandar mensagem para https://www.linkedin.com/in/leonardo-luiz-antunes/

Pré-requisitos:

Instalação do NPM e Yarn

`npm install`

e

`npm install --global yarn`

NodeModules está no GitIgnore, portanto, será necessário a instalação do Cypress com o Cucumber.

Para realizar esta instalação, utilizar o comando:

`npm install --save-dev cypress cypress-cucumber-preprocessor`

Também necessário realizar a instalação do Mochawesome. Utilize o seguinte comando:

`npm i --save-dev cypress-mochawesome-reporter`

Após instalação, rodar o comando abaixo para rodar o cypress pela primeira vez.

`npx cypress open`

Posteriormente, para utilizar os reports do Mochawesome, bata executar o comando

`cypress run --reporter mochawesome`

