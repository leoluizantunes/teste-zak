
import Cart from '../../support/pageObjects/cartPage'
import Itens from '../../support/pageObjects/itensPage'


When(/^adiciono um item ao carrinho e entro no carrinho$/, () => {
    Itens.addBackpack()
    Itens.filterLoHi()
    Itens.addOnesie()
    Itens.cartButton()
	
});

When(/^clico em checkout e insiro as informações de entrega$/, () => {
	Cart.itemValidation()
    Cart.inputInformation()
});

Then(/^clico em finish para finalizar a compra e o status apresentado é Complete!$/, () => {
	Cart.overviewValidation()
    Cart.totalCalculate()
    Cart.finishPurchase()
    Cart.purchaseValidation()
});
