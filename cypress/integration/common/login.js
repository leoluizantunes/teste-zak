import Login from '../../support/pageObjects/loginPage'

Given(/^acesso o site e logo$/, () => {
    Login.acessLoginPage()
    Login.inputCredential()
    Login.clickLoginButton()
});
