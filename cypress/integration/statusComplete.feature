Feature: Status "Complete"

    Background:
        Given acesso o site e logo

    Scenario: Mostrar status "Complete" após a compra
        When adiciono um item ao carrinho e entro no carrinho
        And clico em checkout e insiro as informações de entrega
        Then clico em finish para finalizar a compra e o status apresentado é Complete!

    

    