export const itensElements = {

    //itens a serem adicionados na mochila
    addToCartBackpack:'[data-test="add-to-cart-sauce-labs-backpack"]',
    addToCartItemSauceLabsOnesie: '[data-test="add-to-cart-sauce-labs-onesie"]',
    
    //filtro de busca
    filterButton: '[data-test=product_sort_container]',
    
    //botão do carrinho
    cart:'[class="shopping_cart_link"]',

    
}

