export const carElements = {

    //validação dos itens

    itemBackpackValidate: '[id="item_4_title_link"]',
    itemOnesieValidate: '[id="item_2_title_link"]',
    
    //informações pessoais
    firstnameField:'[data-test="firstName"]',
    lastnameField:'[data-test="lastName"]',
    postalCodeField:'[data-test="postalCode"]',
    firstname: 'Leo',
    lastname: 'Luiz',
    postalCode: '04446160',

    //botões
    checkoutButton: '[data-test="checkout"]',
    continueButton:'[data-test="continue"]',
    finishButton: '[data-test="finish"]',
    
    //status do carrinho
    status:'[class="title"]',

    //valores
    subTotal: '[class="summary_subtotal_label"]',
    tax: '[class="summary_tax_label"]',
    total: '[class="summary_total_label"]',
    priceItemOne: ':nth-child(3) > .cart_item_label > .item_pricebar > .inventory_item_price',
    priceItemTwo: ':nth-child(4) > .cart_item_label > .item_pricebar > .inventory_item_price'

    

}