
import { itensElements } from './../../support/elements/itensElements';

class Itens {

    //adiciona a mochila no carrinho
    addBackpack(){
        cy.get(itensElements.addToCartBackpack).click()
    }

    //adiciona a roupa de criança na mochila
    addOnesie(){
        cy.get(itensElements.addToCartItemSauceLabsOnesie).click()
    }

    //filtra para buscar pelo menor valor
    filterLoHi(){
        cy.get(itensElements.filterButton).select('lohi')
    }

    //botão do carrinho
    cartButton(){
        cy.get(itensElements.cart).click()
    }

}

export default new Itens();