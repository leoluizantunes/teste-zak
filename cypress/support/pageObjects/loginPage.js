
import { loginElements } from './../../support/elements/loginElement';


class Login {

    //acessar página de login
    acessLoginPage(){
        cy.visit('/')
    }

    //inserir credenciais
    inputCredential(){
        cy.fixture('credential').then((credential) => {
            const username = credential.username
            const password = credential.password
            cy.get(loginElements.usernameField).type(username)
            cy.get(loginElements.passwordField).type(password)
        })
    }

    //clicar no botão login e validar
    clickLoginButton(){
        cy.get(loginElements.loginButton).click()
        cy.url().should('include', 'inventory')

    }

}

export default new Login();