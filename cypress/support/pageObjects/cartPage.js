
import { expect } from "chai";
import { carElements } from "../elements/cartElements";

class Cart {

    itemValidation(){

        //validar se entrou no carrinho
        cy.get(carElements.status).should('have.text', 'Your Cart')

        //validar se os itens foram adicionados ao carrinho
        cy.get(carElements.itemBackpackValidate).should('exist')
        cy.get(carElements.itemOnesieValidate).should('exist')

        //clicar no botão checkout
        cy.get(carElements.checkoutButton).click()
        
    }

    inputInformation(){

        //verificar se entrou na página de inclusão de informação
        cy.get(carElements.status).should('have.text', 'Checkout: Your Information')

        //adicionar informações pessoais
        cy.get(carElements.firstnameField).type(carElements.firstname)
        cy.get(carElements.lastnameField).type(carElements.lastname)
        cy.get(carElements.postalCodeField).type(carElements.postalCode)

        //clicar no botão continuar
        cy.get(carElements.continueButton).click()

    }

    overviewValidation(){

        //validar se entrou na página de overview
        cy.get(carElements.status).should('have.text', 'Checkout: Overview')


    }

    finishPurchase(){

         //clicar no botão finalizar para finalizar a compra
         cy.get(carElements.finishButton).click()
    }

    purchaseValidation(){

        //verificar se a compra foi completa com sucesso
        cy.get(carElements.status).should('have.text', 'Checkout: Complete!')

    }

    totalCalculate(){

        //calcular e validar o preço corretamente

        //transforma o valor apresentado do primeiro item de string em float
        cy.get(carElements.priceItemOne).invoke('text').then(($firstItemPrice) => {
            $firstItemPrice = $firstItemPrice.replace(/\D/g,'')
            const firstItem = parseFloat($firstItemPrice)

            //transforma o valor apresentado do segundo item de string em float
            cy.get(carElements.priceItemTwo).invoke('text').then(($secondItemPrice) => {
                $secondItemPrice = $secondItemPrice.replace(/\D/g,'')
                const secondItem = parseFloat($secondItemPrice)

                //soma os dois itens
                const sumItemPrice = firstItem + secondItem

                //transforma o valor apresentado no subtotal do site de string em float
                cy.get(carElements.subTotal).invoke('text').then(($itemSubTotal) => {
                    $itemSubTotal = $itemSubTotal.replace(/\D/g,'')
                    const subTotal = parseFloat($itemSubTotal)

                    //assertion para verificar se o valor do subtotal apresentado no site é igual a soma dos itens do carrinho
                    expect(sumItemPrice).to.equal(subTotal)

                    //transforma o valor da taxa apresentada no site de string em float
                    cy.get(carElements.tax).invoke('text').then(($taxPrice) => {
                        $taxPrice = $taxPrice.replace(/\D/g,'')
                        const tax = parseFloat($taxPrice)

                        //soma o valor dos itens (somados anteriormente) ao valor da taxa
                        const totalPurchasePrice = sumItemPrice + tax

                        //transforma o valor total apresentado no site de string em float
                        cy.get(carElements.total).invoke('text').then(($totalPrice) => {
                            $totalPrice = $totalPrice.replace(/\D/g,'')
                            const total = parseFloat($totalPrice)

                            //assertion para verificar se o valor total apresentado no site é igual ao valor total dos itens somados a taxa
                            expect(totalPurchasePrice).to.equal(total)
                        })
                    })
                })
            })       
        })      
    }
}

export default new Cart();